# git-watch
Python script that watches your starred GitHub repositories
and notifies you about new releases via push notifications.

> **Attention!** If you used `gitwatch` before v2.0, you may want to clear your releases file.
> Since v2.0 the repositories' full name is used instead of just the repository name, i.e. albalitz/git-watch instead of git-watch, which means if you don't clear your releases file, you end up with duplications.

## Requirements
1. Install Python requirements: `pip install -r requirements.txt`
1. Create an account for one of the supported notification services.
1. Create a GitHub [access token](https://github.com/settings/tokens).
1. Copy config-example.json to config.json and add/update your configuration as needed.

Supported notification services:

- `notify-send` on Linux devices  
  PRs for other platforms welcome
- [simplepush.io](https://simplepush.io/)  
  **Note**: You can use simplepush for free for 7 days. _After the 7-day-trial the app requires a one-time purchase._
- [pushbullet.com](https://www.pushbullet.com/)
- [Mattermost](https://about.mattermost.com/)

The script always tries to run `notify-send` along with a specified external notification service, unless run with `--disable-system`.

## Usage Examples
### Cron
You can run this script from a cron job to periodically check for new releases.

To check once per hour, use `crontab -e` to add this line to your crontab:
```shell
0 * * * * export DISPLAY=:0.0 && /path/to/gitwatch.py
```
(Replace the path with the actual path on your system)

`export DISPLAY=:0.0` is required for `notify-send`-based system notifications.

### Docker
You may also run `git-watch` in a Docker container.
To do so, you'll first want to prepare your configuration file and a location for the releases to be saved persistently outside of the container.  
Make sure to set the `releases_file` location in your config.json to `/etc/.gitwatch_releases.json`.

Then build the image and run it:
```shell
docker build --tag git-watch https://raw.githubusercontent.com/albalitz/git-watch/master/Dockerfile
docker run --name git-watch -v /path/to/.gitwatch_releases.json:/etc/.gitwatch_releases.json -v /path/to/config.json:/git-watch/gitwatch/config.json git-watch
```

To run the script again, you can simply restart the container: `docker start git-watch`
