FROM python:3.6

ENV GIT_WATCH_VERSION v2.2
ENV GIT_WATCH_DIR /git-watch

RUN apt-get update && apt-get install -y --no-install-recommends git python3-dev

RUN git clone --branch $GIT_WATCH_VERSION https://github.com/albalitz/git-watch.git $GIT_WATCH_DIR \
    && pip install -r $GIT_WATCH_DIR/requirements.txt

VOLUME $GIT_WATCH_DIR/gitwatch/config.json
VOLUME /etc/.gitwatch_releases.json

ENTRYPOINT python $GIT_WATCH_DIR/gitwatch/gitwatch.py -v --disable-system
