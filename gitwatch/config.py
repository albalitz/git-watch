import os

from wasserstoff import Config, Environment


gitwatch_dir = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(gitwatch_dir, "config")

conf = Environment()
conf.patch(Config(
    filename = config_file
))
conf.commit()

CONFIG = conf.default
