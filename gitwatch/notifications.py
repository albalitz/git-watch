import requests
from logging import getLogger

import sh
from simplepush import send_encrypted
from mattersend import send as send_mattermost

from config import CONFIG

logger = getLogger(__name__)


def _system(title, msg):
    if CONFIG.disable_system:
        return

    try:
        sh.notify_send(title, msg)
    except Exception as e:
        # todo: cross platform
        logger.warning("notify-send not supported.")


def _mattermost(title, msg):
    mm_config = CONFIG.SECRETS["mattermost"]
    try:
        username = mm_config["username"]
    except KeyError:
        username = ""

    send_mattermost(
        channel = mm_config["channel"],
        url = mm_config["webhook_url"],
        message = f"{title}\n\n{msg}",
        username = username
    )

def _pushbullet(title, msg):
    pb_config = CONFIG.SECRETS["pushbullet"]
    headers = {
        "Access-Token": pb_config["token"],
        "Content-Type": "application/json"
    }
    payload = {
        "title": title,
        "body": msg,
        "type": "note",
        "email": pb_config["email"]
    }
    response = requests.post("https://api.pushbullet.com/v2/pushes", json=payload, headers=headers)
    if response.status_code != 200:
        logger.error(f"Pushbullet returned with status code {response.status_code}. Please check your pushbullet secrets.")
        logger.debug(response.json())


def _simplepush(title, msg):
    sp_config = CONFIG.SECRETS["simplepush"]
    try:
        send_encrypted(sp_config["token"], sp_config["password"], sp_config["salt"], title, msg, sp_config["event_id"])
    except KeyError:
        send_encrypted(sp_config["token"], sp_config["password"], sp_config["salt"], title, msg)


def notify(repo, new_release):
    logger.debug(f"Sending notification for {repo.name} {new_release}")
    title = f"New release for {repo.full_name}!"
    msg = f"{repo.full_name} has a new release: {new_release}\n{repo.html_url}"

    _notify = None
    if CONFIG.NOTIFICATION_SERVICE == "pushbullet":
        _notify = _pushbullet
    elif CONFIG.NOTIFICATION_SERVICE == "simplepush":
        _notify = _simplepush
    elif CONFIG.NOTIFICATION_SERVICE == "mattermost":
        _notify = _mattermost

    if _notify is not None:
        try:
            _notify(title, msg)
        except KeyError as e:
            logger.error(e)

    _system(title, msg)
