#!/usr/bin/env python3
__version__ = "2.0"

import argparse
from time import sleep
from socket import timeout

from github import Github

import logging.config

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
DEFAULT_LOGLEVEL = logging.WARNING
logger = logging.getLogger(__name__)


from config import CONFIG
from persistence import read_last_releases
from persistence import update_releases
from notifications import notify


RATE_LIMIT_RELEASE_TIME = 60 * 10


def check_releases():
    logger.info("Checking releases...")
    if not CONFIG.NOTIFICATION_SERVICE:
        logger.warning("No notification service specified. You won't receive any external notifications.")

    gh = Github(login_or_token = CONFIG.SECRETS["github"]["token"])
    gh_user = gh.get_user(CONFIG.GITHUB_USER)

    change_count = 0
    for repo in gh_user.get_starred():
        sleep(1)
        remaining_rates = gh.get_rate_limit().rate.remaining
        if remaining_rates <= 20:
            logger.warning(f"API rate limiting! Remaining: {remaining_rates}. Waiting {RATE_LIMIT_RELEASE_TIME} seconds...")
            sleep(RATE_LIMIT_RELEASE_TIME)

        known_releases = read_last_releases()
        try:
            current_repo_releases = [tag.name for tag in repo.get_tags()]
        except timeout:
            logger.error(f"Timeout while trying to get current repo tags: {repo.full_name}")
            continue
        has_new_release = False
        if not known_releases.get(repo.full_name):
            has_new_release = True
        else:
            if current_repo_releases != known_releases.get(repo.full_name, []):
                has_new_release = True

        known_releases[repo.full_name] = current_repo_releases
        try:
            newest_release = current_repo_releases[0]
        except IndexError:
            has_new_release = False

        if has_new_release:
            change_count += 1
            logger.info(f"New release for {repo.full_name}: {newest_release}")
            notify(repo, newest_release)
            update_releases(known_releases)

    logger.info(f"{change_count} new version{'s' if change_count != 1 else ''} found.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Release notifications for starred repositories on GitHub.")

    parser.add_argument(
        "--disable-system",
        action = "store_true",
        help = "Disable system notifications."
    )

    parser.add_argument(
        "-v", "--verbose",
        action = "count",
        help = "Increase the loglevel."
    )
    parser.add_argument(
        "--version",
        action = "store_true",
        help = "Display the script's version and exit."
    )

    args = parser.parse_args()

    if args.version:
        print(f"git-watch v{__version__}")
        quit()

    log_level = DEFAULT_LOGLEVEL - args.verbose * 10 if args.verbose else DEFAULT_LOGLEVEL
    logging.basicConfig(level=log_level, format=LOG_FORMAT)

    CONFIG.disable_system = args.disable_system
    if CONFIG.disable_system:
        logger.info("Running without system notifications.")

    check_releases()
