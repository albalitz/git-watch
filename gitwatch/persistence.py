import os
import json

from config import CONFIG


def _releases_file():
    return os.path.expandvars(CONFIG.RELEASES_FILE)


def read_last_releases():
    """Reads the releases file and returns the contained releases.

    Returns:
        A dict of repository releases.
    """
    repository_releases = {}

    if not os.path.exists(_releases_file()):
        with open(_releases_file(), "w+") as f:
            f.write("{}")

    with open(_releases_file(), "r") as f:
        repository_releases = json.loads("".join(f.readlines()))

    return repository_releases


def update_releases(repository_releases):
    """Update the given repositories' releases.

    Takes a list of repositories and updates them in the releases file.

    Args:
        repository_releases: List of repositories with updated releases.
    """
    with open(_releases_file(), "w+") as f:
        f.write(json.dumps(repository_releases, indent=4))
